<?php namespace volvoRennes;

use Illuminate\Database\Eloquent\Model;

class Marque extends Model {

	protected $fillable = ['label', 'picture'];
    
	protected $table = 'carlabel';
    
	protected $hidden = '';

}
