<?php 

namespace volvoRennes\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use volvoRennes\Marque;
use volvoRennes\Modele;


class adminVolvoController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| AdminVolvo Controller
	|--------------------------------------------------------------------------
	|
	| Controller destiné à l'espace d'administration volvo.
	|
	*/

	public function __construct()
	{
		$this->middleware('guest');
	}
        
    /* HOME PAGE */
        
	public function index()
	{
		return view('admin/adminHome');
	}
        
        /* VEHICULES */
        
        public function addNewCar()
        {
            return view('admin/addNewCar');
        }
        
            /* MARQUE ET MODELES */
        
            public function addModel()
            {
                $marques = Marque::all();
                $modeles = [];
                
                foreach ($marques as $marque) {
                    $models = Modele::where('marque', '=', $marque->id)->get();
                    if (sizeof($models) > 0) {
                        $modeles[$marque->id] = $models;
                    } else {
                        $modeles[$marque->id] = FALSE;
                    }
                }
                
                return view('admin/addModel')->with([
                    'marques' => $marques,
                    'modeles' => $modeles
                ]);
            }

        
            public function removeModele($id)
            {
                $modele = Modele::find($id);
                
                $nom = $modele->model;
                
                $marque = Marque::find($modele->marque);
                $marque = $marque->label;

                $modele->delete();
                
                return redirect()->route('addModel')->with('success', 'Le modèle '. $nom .' à bien été supprimé de : '.$marque);
            }
        
            public function removeMarque($id)
            {
                
            }

            public function registerModel(Request $req)
            {
                
                
                $params = $req->except(['_token']);
                
                switch ($params['action']) {
                        
                    case 'marque':
                        $extension = $params['marquePhoto']->getClientOriginalExtension();
                        $marque = $params['marque']; 
                        $imageName = Str::slug($marque) . '.' . $extension;
                        $imageFolder = base_path() . '/public/images/marque/';
                        $imageBDD = 'images/marque/' . $imageName;
                                                
                        $params['marquePhoto']->move( $imageFolder, $imageName );
                        
                        Marque::create([
                            'label' => $params['marque'],
                            'picture' => $imageBDD
                        ]);

                        return redirect()->route('addModel')->with('success', 'La marque '. $marque .' à bien été ajouté');
                        
                    break;
                        
                    case 'modele':
                        
                        Modele::create([
                            'marque' => $params['marque'],
                            'model' => $params['modele'],
                            'perso1' => 'N',
                            'perso2' => 'N'
                        ]);
                        
                        $modele = $params['modele'];
                        
                        $marque = Marque::find($params['marque']);
                        $marque = $marque->label;
                        
                        return redirect()->route('addModel')->with('success', 'Le modèle '.$modele.' à bien été ajouté à '.$marque);
                        
                    break;
                        
                    case 'modeleModif':
                                                
                        $modele = Modele::find($params['id']);
                                                
                        $modele->model = $params['modele'];
                        
                        $modele->save();
                        
                        $marque = Marque::find($modele->marque);
                        $marque = $marque->label;
                        
                        $modele = $modele->model;
                        
                        return redirect()->route('addModel')->with('success', 'Le modèle '.$params['oldModele'].' est devenu '.$modele.' dans : '.$marque);
                        
                    break;

                }
                
            } 

}
