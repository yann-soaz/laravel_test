<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*
ADMIN
*/

Route::get('/admin', [
    'as' => 'adminHome',
    'uses' => 'adminVolvoController@index'
]);

    /*
    VEHICULES
    */

    Route::get('/admin/vehicule-neuf/ajouter', [
        'as' => 'addNewCar',
        'uses' => 'adminVolvoController@addNewCar'
    ]);

        /*
        MARQUES ET MODELES
        */

        Route::get('/admin/marques-et-modeles', [
            'as' => 'addModel',
            'uses' => 'adminVolvoController@addModel'
        ]);

            /*
            ENREGISTREMENT ET MODIFICATION
            */
            Route::post('/admin/marques-et-modeles/enregistrer', [
                'as' => 'registerModel',
                'uses' => 'adminVolvoController@registerModel'
            ]);

            /*
            SUPPRESSION
            */
            Route::get('/admin/marques-et-modeles/supprimer-modele/{id}', [
                'as' => 'removeModel',
                'uses' => 'adminVolvoController@removeModele'
            ]);

            Route::get('/admin/marques-et-modeles/supprimer-marque/{id}', [
                'as' => 'removeMarque',
                'uses' => 'adminVolvoController@removeMarque'
            ]);
