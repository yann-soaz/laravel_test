<?php namespace volvoRennes;

use Illuminate\Database\Eloquent\Model;

class Modele extends Model {

	protected $fillable = ['marque', 'model', 'perso1', 'perso2'];
    
	protected $table = 'carmodel';
    
	protected $hidden = '';

}
