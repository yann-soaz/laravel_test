@extends('admin/adminApp')

@section('titre')
Ajouter une voiture au stock
@endsection

@section('content')

<form class="col s12">
    <div class="col s12">
        <h5 class="formH">Description générale</h5>

        <div class="input-field col m6">
            <select name="modele">
                <option value="" disabled selected>Selectionnez un modèle</option>
                <option value="1">C40</option>
                <option value="2">M90</option>
                <option value="3">O10</option>
            </select>
        </div>

        <div class="input-field col m6">
              <input id="Garantie" type="number" name="garantie" value="24">
              <label for="Garantie">Durée de garantie (en mois)</label>
        </div>

        <div class="input-field col m6">
              <input id="serie" name="serie" type="text">
              <label for="serie">Série</label>
        </div>

        <div class="input-field col m6">
            <select name="finition">
                <option value="" disabled selected>Finition</option>
                <option value="1">FEELING</option>
                <option value="2">MOMENTUM</option>
                <option value="3">SPORT</option>
            </select>
        </div>

        <div class="input-field col m6">
              <input id="serie" name="serieNumber" type="text">
              <label for="serie">Numéro de série</label>
        </div>
    </div>
    
    <div class="col m6">
        <h5 class="formH">Mécanique</h5>
        <div class="col s12 oneChecked">
            <p class="col s12">
                Carburation :
            </p>
            <p class="col s6">
              <input type="checkbox" id="essence" value="essence" name="carburation" checked />
              <label for="essence">essence</label>
            </p>
            <p class="col s6">
              <input type="checkbox" id="diesel" value="diesel" name="carburation" />
              <label for="diesel">diesel</label>
            </p>
            <p class="col s6">
              <input type="checkbox" id="gpl" value="gpl" name="carburation" />
              <label for="gpl">gpl</label>
            </p>
            <p class="col s6">
              <input type="checkbox" id="biocarburant" value="biocarburant" name="carburation" />
              <label for="biocarburant">biocarburant</label>
            </p>
            <p class="col s6">
              <input type="checkbox" id="hybride" value="hybride" name="carburation" />
              <label for="hybride">hybride</label>
            </p>
            <p class="col s6">
              <input type="checkbox" id="electrique" value="electrique" name="carburation" />
              <label for="electrique">électrique</label>
            </p>
        </div>

        <div class="input-field col s12">
              <input id="co2" name="co2" type="text">
              <label for="co2">Taux de CO2 (en g/km )</label>
        </div>

        <div class="col s12 oneChecked">
            <p class="col s12">
                Boîte :
            </p>
            <p class="col s6">
              <input type="checkbox" id="manuelle" value="manuelle" name="boite" checked />
              <label for="manuelle">manuelle</label>
            </p>
            <p class="col s6">
              <input type="checkbox" id="auto" value="auto" name="boite" />
              <label for="auto">auto</label>
            </p>
        </div>
    </div>
    
    <div class="col m6">
        <h5 class="formH">Carrosserie/Intérrieur</h5>
        <div class="input-field col s12">
              <input id="nbPorte" name="nbPorte" type="number">
              <label for="nbPorte">Nombre de porte</label>
        </div>
        <div class="input-field col s12">
              <input id="interieur" name="interieur" type="text">
              <label for="interieur">Intérieur</label>
        </div>
        <div class="input-field col s12">
              <input id="carrosserie" name="carrosserie" type="text">
              <label for="carrosserie">Carrosserie</label>
        </div>
    </div>

    <div class="col s12">
        <h5 class="formH">Options</h5>
        <div class="col m4 s6">
            <p>Confort :</p>
            <div class="col s12 oneChecked">
                <p class="col m12 s6">
                  <input type="checkbox" id="manuelle" value="manuelle" name="boite" checked />
                  <label for="manuelle">manuelle</label>
                </p>
                <p class="col m12 s6">
                  <input type="checkbox" id="auto" value="auto" name="boite" />
                  <label for="auto">auto</label>
                </p>
            </div>
        </div>
        
        <div class="col m4 s6">
            <p>Assistance à la conduite :</p>
            <div class="col s12 oneChecked">
                <p class="col m12 s6">
                  <input type="checkbox" id="manuelle" value="manuelle" name="boite" checked />
                  <label for="manuelle">manuelle</label>
                </p>
                <p class="col m12 s6">
                  <input type="checkbox" id="auto" value="auto" name="boite" />
                  <label for="auto">auto</label>
                </p>
            </div>
        </div>
        
        <div class="col m4 s6">
            <p>Autres :</p>
            <div class="col s12">
                
            </div>
            <div class="input-field col s12">
                <textarea id="textarea1" class="materialize-textarea"></textarea>
                <label for="textarea1">Textarea</label>
            </div>
        </div>
    </div>
    
    <div class="col s12">
        <div class="file-field center-align input-field col m4">
            <div class="btn col s12">
                <span>Photo</span>
                <input type="file" name="marquePhoto[]" class="previewImage">
            </div>
            <img src="" data-image="" class="responsive-img" />
        </div>
        
        <div class="file-field center-align input-field col m4">
            <div class="btn col s12">
                <span>Photo</span>
                <input type="file" name="marquePhoto[]" class="previewImage">
            </div>
            <img src="" data-image="" class="responsive-img" />
        </div>
        
        <div class="file-field center-align input-field col m4">
            <div class="btn col s12">
                <span>Photo</span>
                <input type="file" name="marquePhoto[]" class="previewImage">
            </div>
            <img src="" data-image="" class="responsive-img" />
        </div>
    
    </div>
    
    <div class="col s12 center-align validBox">
        <button class="btn waves-effect waves-light" type="submit" name="action">Enregistrer
            <i class="volvo-checked text-brown text-lighten-5 right"></i>
        </button>
    </div>
</form>

@endsection