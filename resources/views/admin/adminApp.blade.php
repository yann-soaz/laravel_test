<!DOCTYPE html>
<html>

<head>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{!! asset('css/materialize.min.css') !!}" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="{!! asset('css/appAdmin.css') !!}" media="screen,projection" />
    
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
    
    <title>Admin Volvo Rennes</title>
</head>

<body>
    <nav class="brown lighten-1" id="adminNav">
        <div class="container">
            <div class="nav-wrapper">
                <a href="{{ route('adminHome') }}" class="brand-logo"><img src="{!! asset('images/logo.svg') !!}" class="navLogo" /></a>
                <a href="#" data-activates="responsive-nav" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="/">Voir le site</a></li>
                    <li>
                        <a class="dropdown-button" data-activates="Vehicules" href="#!">
                            Gestion des véhicules<i class="material-icons right">arrow_drop_down</i>
                        </a>
                        <ul id="Vehicules" class="brown lighten-2 dropdown-content">
                          <li><a class="brown-text text-lighten-5" href="#!">En stock</a></li>
                          <li><a class="brown-text text-lighten-5" href="#!">Occasions</a></li>
                          <li class="divider"></li>
                          <li><a class="brown-text text-lighten-5" href="{{ route('addModel') }}">Modèles</a></li>
                          <li><a class="brown-text text-lighten-5" href="#!">Options</a></li>
                        </ul>
                    </li>
                    <li><a href="#!">menu</a></li>
                    <li><a href="#!">menu</a></li>
                </ul>
            </div>
        </div>
    </nav>
    
    <div class="container">
        <div class="row">            
            <header class="col s12 valign-wrapper">
                <div class="col m4 center-align">
                    <img src="{!! asset('images/logo.svg') !!}" class="logoImg responsive-img" />
                </div>
                <div class="col m8 center-align valign">
                    <h3>@yield('titre')</h3>
                    @if(Session::has('success'))
                        <div class="card-panel light-green lighten-2 center-align grey-text text-darken-4">{{ Session::get('success') }}</div>
                    @elseif(Session::has('error'))
                        <div class="alert alert-danger">{{ Session::get('error') }}</div>
                    @endif
                </div>
            </header>
        </div>
    </div>
    <div class="container">
        <div class="row">            
            @yield('content')
        </div>
    </div>
    
    
    <footer class="page-footer brown lighten-1">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="white-text">Volvo Defrance</h5>
                    <p class="grey-text text-lighten-4">AUTOPÔLE CESSON-SÉVIGNÉ</p>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container center-align">
                © 2017 <a href="http://www.useweb.fr/" class="brown-text text-lighten-5">UseWeb</a>
            </div>
        </div>
    </footer>
    
    <ul class="side-nav" id="responsive-nav">
        <li><a href="{{ route('adminHome') }}"><img src="images/logo.svg" class="navLogo" /></a></li>
        <li><a href="/">Voir le site</a></li>
        <li><a href="#!">menu</a></li>
        <li><a href="#!">menu</a></li>
        <li><a href="#!">menu</a></li>
    </ul>
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="{!! asset('js/materialize.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/adminApp.js') !!}"></script>
</body>

</html>