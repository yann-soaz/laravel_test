@extends('admin/adminApp')

@section('titre')
Les marques et modèles
@endsection

@section('content')



@if(sizeof($marques) > 0)
<ul class="collapsible popout" data-collapsible="accordion">
    @foreach($marques as $marque)
        <li>
            <div class="collapsible-header">
                <div class="row">
                    <div class="col l2 m4 hide-on-small-only valign-wrapper">
                        <img src="{!! asset($marque->picture) !!}" class="responsive-img valign">
                    </div>
                    <div class="col s12 m8 l10 valign-wrapper">
                        <h5 class="black-text valign">
                            {{ $marque->label }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="collapsible-body">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            @if($modeles[$marque->id])
                                
                                <ul class="collection">
                            
                                @foreach($modeles[$marque->id] as $modele )
                                    <li class="collection-item hoverable">
                                        <div>
                                            {{ $modele->model }}
                                            <a href="#modalDelete" class="deleteModel red-text text-darken-2 right" title="supprimer" data-url=" {{ route('removeModel', ['id' => $modele->id]) }}" data-modele="{{ $modele->model }}">
                                                <i class="volvo-rubbish-bin"></i>
                                            </a>
                                            <a href="#modalModif" class="modify light-blue-text text-darken-4 right" title="modifier" data-id="{{ $modele->id }}" data-modele="{{ $modele->model }}">
                                                <i class="volvo-edit"></i>
                                            </a>
                                        </div>
                                    </li>
                                    
                                @endforeach
                                    
                                </ul>
                            @endif
                        </div>
                        <form class="col s12" method="post" action="{{ route('registerModel') }}" enctype="multipart/form-data">

                                <h5 class="formH">Ajouter un modèle</h5>

                                <div class="input-field col m6 s12">
                                      <input id="modele" name="modele" type="text">
                                      <label for="modele">Modèle</label>
                                </div>

                            <div class="col m6 s12 center-align validBox">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="marque" value="{{ $marque->id }}" />
                                <button class="btn waves-effect waves-light" type="submit" name="action" value="modele">Enregistrer
                                    <i class="volvo-checked text-brown text-lighten-5 right"></i>
                                </button>
                            </div>
                        </form>
                    </div>                
                </div>
            </div>
        </li>
    @endforeach
</ul>
@endif

<form class="col s12" method="post" action="{{ route('registerModel') }}" enctype="multipart/form-data">
        
        <h5 class="formH">Ajouter une marque</h5>

        <div class="file-field center-align input-field col m4 s12">
            <div class="btn col s12">
                <span>Logo</span>
                <input type="file" name="marquePhoto" class="previewImage">
            </div>
            <img src="" data-image="" class="responsive-img" />
        </div>
        
        <div class="input-field col m8 s12">
              <input id="marque" name="marque" type="text">
              <label for="marque">Marque</label>
        </div>
    
    
    <div class="col s12 center-align validBox">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <button class="btn waves-effect waves-light" type="submit" name="action" value="marque">Enregistrer
            <i class="volvo-checked text-brown text-lighten-5 right"></i>
        </button>
    </div>
</form>


<div id="modalModif" class="modal">
    <div class="modal-content">
        
        <form class="row" method="post" action="{{ route('registerModel') }}" enctype="multipart/form-data">
            
                <h5 class="formH">Modifier un modèle</h5>

                <div class="input-field col m6 s12">
                      <input id="modeleModif" name="modele" type="text">
                      <label for="modele">Modèle</label>
                </div>

            <div class="col m6 s12 center-align validBox">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" id="idModele" name="id" value="" />
                <input type="hidden" id="oldModele" name="oldModele" value="" />
                <button class="btn waves-effect waves-light" type="submit" name="action" value="modeleModif">Enregistrer
                    <i class="volvo-checked text-brown text-lighten-5 right"></i>
                </button>
            </div>
            
        </form>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn waves-effect waves-light red lighten-2">Annuler</a>
    </div>
</div>

<div id="modalDelete" class="modal">
    <div class="modal-content">
        
        <div class="container">
            <h5 class="formH">Supprimer un modèle</h5>

            <p>
                  Voulez vous vraiment supprimer <span id="deleteModel"></span> ?
            </p>
        </div>

    </div>
    <div class="modal-footer">
        <a href="#!" class="btn waves-effect waves-light" id="deleteButton">Supprimer</a>
        <a href="#!" class="modal-action modal-close btn waves-effect waves-light red lighten-2">Annuler</a>
    </div>
</div>

@endsection