@extends('admin/adminApp')
@section('titre')
Bienvenu
@endsection

@section('content')


<div class="col s12 hide-on-small-only">
    
    <ul class="tabs">
        <li class="tab col m6 l3"><a href="#concession">La concession</a></li>
        <li class="tab col m6 l3"><a href="#sitevolvo">Le site</a></li>
        <li class="tab col m6 l3"><a class="active" href="#vehicules">Les véhicules</a></li>
        <li class="tab col m6 l3"><a href="#galleries">Galleries</a></li>
    </ul>

    <div id="concession" class="col s12">
        
    </div>
    
    <div id="sitevolvo" class="col s12">

    </div>
    
    <div id="vehicules" class="col s12">
      <div class="row">
          
        <div class="col s12 m6">
          <div class="card hoverable">
            <div class="card-content">
              <span class="card-title">Véhicules neufs</span>
            </div>
            <div class="card-action">
              <a href="{{ route('addNewCar') }}">Ajouter</a>
              <a href="#">Gérer</a>
            </div>
          </div>
        </div>
          
        <div class="col s12 m6">
          <div class="card hoverable">
            <div class="card-content">
              <span class="card-title">Véhicules d'occasion</span>
            </div>
            <div class="card-action">
              <a href="#">Ajouter</a>
              <a href="#">Gérer</a>
            </div>
          </div>
        </div>
          
        <div class="col s12 m6">
          <div class="card hoverable">
            <div class="card-content">
              <span class="card-title">Grille de tarifs</span>
            </div>
            <div class="card-action">
              <a href="#">Gérer</a>
            </div>
          </div>
        </div>
          
        <div class="col s12 m6">
          <div class="card hoverable">
            <div class="card-content">
              <span class="card-title">Les marques &amp; Modèles</span>
            </div>
            <div class="card-action">
              <a href="{{ route('addModel') }}">Gérer</a>
            </div>
          </div>
        </div>
          
      </div>
    </div>
    <div id="galleries" class="col s12">
      <a class="btn dropdown-button" href="#!" data-activates="dropdown2">Dropdown<i class="mdi-navigation-arrow-drop-down right"></i></a>
      <a class="btn dropdown-button" href="#!" data-activates="dropdown2">Dropdown<i class="mdi-navigation-arrow-drop-down right"></i></a>
      <a class="btn dropdown-button" href="#!" data-activates="dropdown2">Dropdown<i class="mdi-navigation-arrow-drop-down right"></i></a>
    </div>

</div>
<div class="col s12">

    <ul class="collapsible popout hide-on-med-and-up" data-collapsible="accordion">
        <li>
            <div class="collapsible-header"><i class="material-icons">filter_drama</i>First</div>
            <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
        </li>
        <li>
            <div class="collapsible-header"><i class="material-icons">place</i>Second</div>
            <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
        </li>
        <li>
            <div class="collapsible-header"><i class="material-icons">whatshot</i>Third</div>
            <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
        </li>
    </ul>
    
</div>

@endsection