(function ($) {
    
    $.fn.unanChecked = function () {
        return this.each(function(){
            var ensemble = $(this),
                inputs = ensemble.children().children('input');
            inputs.change(function() {
                inputs.not($(this)).each(function () {
                    if ($(this).is(':checked')) {
                        $(this).prop('checked', false);
                    }
                });
            });
        });
    }
    
    $.fn.unanImage = function () {
        this.val('');
        this.on('change', function (e) {
            let files = $(this)[0].files;
            if (files.length > 0) {

                let file = files[0],
                    image_preview = $(this).parent().next('img'),
                    parentBox = $(this).parent().parent();

                image_preview.attr('src', window.URL.createObjectURL(file));
                if (parentBox.children('a.parameterImg').length) {
                    parentBox.children('a.parameterImg').remove();
                }
                parentBox.append('<a href="#!" class="parameterImg col s12 btn waves-effect waves-light red lighten-2"><i class="volvo-rubbish-bin"></i> Annuler</a>');
            }
        });
    }
    
    $.fn.positionBottom = function () {
        let screenHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
            elementPosition = this.offset().top,
            elementHeight = this.outerHeight(),
            elementBottom = elementHeight + elementPosition;
        
        if (elementBottom < screenHeight) {
            this.css({
                'position': 'absolute',
                'bottom': 0,
                'left': 0,
                'width': '100%'
            });
            $('body').css({
                'padding-bottom': elementHeight
            });

        }
    }
    
    $(document).ready(function () {
        
        $(".button-collapse").sideNav();
        
        $('select').material_select();
        
        $('.oneChecked').unanChecked();
    
        $('.previewImage').unanImage();
        
        $('.page-footer').positionBottom();
        
        $('.modal').modal();
        
        $('.modify').click(function () {
            let nom = $(this).attr('data-modele'),
                id = $(this).attr('data-id');
            $('#modeleModif').val(nom);
            $('#modeleModif').next('label').addClass('active');
            $('#idModele').val(id);
            $('#oldModele').val(nom);
        });
        
        $('.deleteModel').click(function () {
            let url = $(this).attr('data-url'),
                nom = $(this).attr('data-modele');
            $('#deleteButton').attr('href', url);
            $('#deleteModel').text(nom);
        });
        
        
        
        $(document).on('click', '.parameterImg', function(e) {
            e.preventDefault();
            
            let input = $(this).prev().prev().children('input'),
                imageOrigin = $(this).prev('img').attr('data-image'),
                image = $(this).prev('img');
            
            input.replaceWith(input.val('').clone(true));
            image.attr('src', imageOrigin);
            $(this).remove();
        });
    });
    
})(jQuery);