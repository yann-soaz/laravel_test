<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarModel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carModel', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('marque');
			$table->string('model');
			$table->string('perso1')->default(NULL);
			$table->string('perso2')->default(NULL);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carModel');
	}

}
