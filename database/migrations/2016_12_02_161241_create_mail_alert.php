<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailAlert extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mailAlert', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('civilité');
			$table->string('nom');
			$table->string('prenom');
			$table->string('entreprise');
			$table->string('mail');
			$table->integer('telephone');
			$table->integer('telephone2');
			$table->string('adresse');
			$table->integer('cp');
			$table->string('ville');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mailAlert');
	}

}
