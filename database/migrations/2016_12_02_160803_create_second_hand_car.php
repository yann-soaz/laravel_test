<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondHandCar extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('secondHandCar', function(Blueprint $table)
		{
			$table->increments('id');
			$table->decimal('prix');
			$table->integer('marque');
			$table->integer('modele');
			$table->string('serie');
			$table->string('numSerie');
			$table->integer('garantie');
			$table->string('carburation');
			$table->string('co2');
			$table->string('boite');
			$table->integer('nbPorte');
			$table->string('interieur');
			$table->string('carrosserie');
			$table->longText('optConfort');
			$table->longText('optConduite');
			$table->longText('optAutre');
			$table->longText('precisions');
			$table->longText('pictures');
			$table->string('url');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('secondHandCar');
	}

}
