<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarifs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tarifs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('modele');
			$table->string('precision');
			$table->integer('co2');
			$table->decimal('kinetic');
			$table->decimal('momentum');
			$table->decimal('summum');
			$table->decimal('xenium');
			$table->decimal('perso1Value');
			$table->decimal('perso2Value');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tarifs');
	}

}
