<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescriptionAlert extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alertDescription', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user');
			$table->boolean('occasion');
			$table->boolean('neuf');
			$table->decimal('prixMax');
			$table->integer('marque');
			$table->integer('modele');
			$table->string('finition');
			$table->string('motorisation');
			$table->string('boite');
			$table->integer('année');
			$table->decimal('prix');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alertDescription');
	}

}
